FROM python:3.9 as requirements-stage

WORKDIR /tmp

RUN pip install poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

# Формируем список зависимостей зависимости
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

# Билдим основной контейнер
FROM python:3.9

# Устанавливаем рабочую директорию
WORKDIR /code/src

# Копируем список зависимостей
COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

# Устанавливаем зависимости
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt