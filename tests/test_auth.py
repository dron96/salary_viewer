from httpx import AsyncClient
from sqlalchemy import insert, select

from auth.models import User
from auth.utils import get_password_hash
from conftest import async_session_maker


async def test_add_user():
    async with async_session_maker() as session:
        stmt = insert(User).values(
            id=1,
            email='admin@admin',
            name='admin',
            surname='admin',
            hashed_password=get_password_hash('admin')
        )
        await session.execute(stmt)
        await session.commit()

        query = select(User.id, User.email, User.name, User.surname)
        result = await session.execute(query)
        expected_result = [(1, 'admin@admin', 'admin', 'admin')]
        assert result.all() == expected_result, 'Пользователь не создан'


async def test_auth(ac: AsyncClient):
    response = await ac.post('/token', data={
        "username": "admin@admin",
        "password": "admin",
    })

    assert response.status_code == 200
    assert response.json()['access_token']
    assert response.json()['token_type'] == 'bearer'
