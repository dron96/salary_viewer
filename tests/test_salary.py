from datetime import datetime, timedelta

from httpx import AsyncClient
from sqlalchemy import insert, select

from salary.models import Salary, SalaryIncrease
from tests.conftest import async_session_maker


async def test_create_salary():
    async with async_session_maker() as session:
        values = ({'id': 1, 'user_id': 1, 'salary': 10000}, {'id': 2, 'user_id': 1, 'salary': 20000})
        stmt = insert(Salary).values(values)
        await session.execute(stmt)
        await session.commit()

        query = select(Salary.id, Salary.user_id, Salary.salary)
        result = await session.execute(query)

        expected_result = [tuple(i.values()) for i in values]
        assert result.all() == expected_result, 'Зарплата не создана'


async def test_get_salaries(ac: AsyncClient, access_token: str):
    headers = {
        "Authorization": f"Bearer {access_token}",
    }
    response = await ac.get("/salaries", headers=headers)

    expected_result = [{'id': 1, 'user_id': 1, 'salary': 10000}, {'id': 2, 'user_id': 1, 'salary': 20000}]

    assert response.status_code == 200
    assert response.json() == expected_result
    assert len(response.json()) == 2


async def test_create_salary_increase():
    async with async_session_maker() as session:
        current_datetime = datetime.now()
        delta = timedelta(minutes=5)
        values = (
            {'id': 1, 'salary_id': 1, 'date': current_datetime + delta, 'increase': 10000},
            {'id': 2, 'salary_id': 2, 'date': current_datetime - delta, 'increase': 20000},
        )
        stmt = insert(SalaryIncrease).values(values)
        await session.execute(stmt)
        await session.commit()

        query = select(SalaryIncrease.id, SalaryIncrease.salary_id, SalaryIncrease.date, SalaryIncrease.increase)
        result = await session.execute(query)

        expected_result = [tuple(i.values()) for i in values]
        assert result.all() == expected_result, 'Зарплата не создана'


async def test_get_salary_increases(ac: AsyncClient, access_token: str):
    async with async_session_maker() as session:
        current_datetime = datetime.utcnow()
        delta = timedelta(days=1)
        values = (
            {'id': 3, 'salary_id': 1, 'date': current_datetime + delta, 'increase': 10000},
        )
        stmt = insert(SalaryIncrease).values(values)
        await session.execute(stmt)
        await session.commit()

    headers = {
        "Authorization": f"Bearer {access_token}",
    }
    salary_id = 1
    response = await ac.get(f"/salaries/{salary_id}", headers=headers)

    expected_result = {
        'id': 1,
        'salary': float(10000),
        'user_id': 1,
        'increase': {
            'id': 3,
            'date': (current_datetime + delta).strftime("%Y-%m-%dT%H:%M:%S.%f"),
            'increase': float(10000)
        }
    }

    assert response.status_code == 200
    assert response.json() == expected_result
