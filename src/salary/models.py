from typing import List

from sqlalchemy import Integer, Float, TIMESTAMP, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from database import Base


class Salary(Base):
    __tablename__ = "salary"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"), nullable=False)
    salary: Mapped[Float] = mapped_column(Float, nullable=False)
    increases: Mapped[List["SalaryIncrease"]] = relationship(backref="salary_increase")


class SalaryIncrease(Base):
    __tablename__ = "salary_increase"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    salary_id: Mapped[int] = mapped_column(ForeignKey("salary.id"), nullable=False)
    date: Mapped[TIMESTAMP] = mapped_column(TIMESTAMP, nullable=False)
    increase: Mapped[Float] = mapped_column(Float, nullable=False)
