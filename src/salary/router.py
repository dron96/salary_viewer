from datetime import datetime

from fastapi import APIRouter, Depends, Path, HTTPException
from sqlalchemy import select, func
from sqlalchemy.ext.asyncio import AsyncSession
from typing import Annotated, List

from starlette import status

from auth.schemas import UserSchema
from auth.utils import get_current_user
from database import get_async_session
from salary.schemas import SalarySchema, SalaryIncreaseSchema, SalaryWithIncreaseSchema
from salary.models import Salary, SalaryIncrease

router = APIRouter(
    prefix='/salaries',
    tags=['Salary']
)


@router.get(
    "",
    response_model=List[SalarySchema],
    summary='Получения списка заработных плат пользователя',
    description='Список, потому что пользователь может быть трудоустроен на нескольких должностях в данной организации',
)
async def get_salaries(
        current_user: Annotated[UserSchema, Depends(get_current_user)],
        session: AsyncSession = Depends(get_async_session),
):
    query = select(Salary).where(Salary.user_id == current_user.id)
    result = await session.scalars(query)
    return result.all()


@router.get(
    "/{salary_id}",
    response_model=SalaryWithIncreaseSchema,
    summary='Получение конкретной заработной платы пользователя с ближайшим повышением',
    description='Так же у заработной платы может быть несколько повышений, поэтому выбираем ближайшее',
)
async def get_salary_increases(
        current_user: Annotated[UserSchema, Depends(get_current_user)],
        salary_id: Annotated[int, Path(title='The ID of the users salary')],
        session: AsyncSession = Depends(get_async_session)
):
    query = select(Salary).where(Salary.id == salary_id)
    result = (await session.scalars(query)).first()
    if result is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
        )
    salary = SalarySchema.from_orm(result)
    if salary.user_id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="It's not your salary",
            headers={"WWW-Authenticate": "Bearer"},
        )
    query = (
        select(SalaryIncrease)
        .where(SalaryIncrease.salary_id == salary_id)
        .where(func.date(SalaryIncrease.date) > datetime.utcnow())
        .order_by(SalaryIncrease.date)
    )
    result = await session.scalars(query)
    result = result.first()
    salary_increase = None
    if result:
        salary_increase = SalaryIncreaseSchema.from_orm(result)
    return {**salary.dict(), "increase": salary_increase}
