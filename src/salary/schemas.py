from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field


class SalaryIncreaseSchema(BaseModel):
    id: int = Field()
    date: datetime = Field(title='Дата и время следующего повышения')
    increase: float = Field(title='Размер повышения заработной платы')

    class Config:
        orm_mode = True


class SalaryWithIncreaseSchema(BaseModel):
    id: int
    salary: float = Field(title='Размер текущей заработной платы')
    user_id: int = Field(title='ID пользователя')
    increase: Optional[SalaryIncreaseSchema] = Field(title='Модель повышения')

    class Config:
        orm_mode = True


class SalarySchema(BaseModel):
    id: int
    user_id: int = Field(title='ID пользователя')
    salary: float = Field(title='Размер текущей заработной платы')

    class Config:
        orm_mode = True
