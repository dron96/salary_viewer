from typing import Union

from pydantic import BaseModel


class UserSchema(BaseModel):
    id: int
    email: str
    name: str
    surname: str

    class Config:
        orm_mode = True


class UserWithPasswordSchema(UserSchema):
    hashed_password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Union[str, None] = None
