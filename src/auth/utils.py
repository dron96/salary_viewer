from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from jose import jwt, JWTError
from sqlalchemy import select
from datetime import timedelta, datetime
from typing import Union, Annotated
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from config import ACCESS_TOKEN_EXPIRE_MINUTES, SECRET_KEY, ALGORITHM
from auth.models import User
from auth.schemas import UserWithPasswordSchema, TokenData
from database import get_async_session

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password):
    print(plain_password)
    print(hashed_password)
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def create_access_token(username: str):
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    return create_token(
        data={"sub": username}, expires_delta=access_token_expires
    )


def create_token(data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def authenticate_user(username: str, password: str, session: AsyncSession):
    user_model = await get_user_from_db(username, session)
    if not user_model:
        return False
    if not verify_password(password, user_model.hashed_password):
        return False
    return user_model


async def get_user_from_db(email: str, session: AsyncSession):
    query = select(User).where(User.email == email)
    result = await session.scalars(query)
    result = result.first()
    if result:
        return UserWithPasswordSchema.from_orm(result)


async def get_current_user(
        token: Annotated[str, Depends(oauth2_scheme)],
        session: AsyncSession = Depends(get_async_session),
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(email=username)
    except JWTError:
        raise credentials_exception
    user_data = await get_user_from_db(token_data.email, session)
    if user_data is None:
        raise credentials_exception
    return user_data
