from fastapi import FastAPI
from auth.router import router as auth_router
from salary.router import router as salary_router

app = FastAPI(
    title="Salary Viewer"
)

app.include_router(auth_router)

app.include_router(salary_router)
